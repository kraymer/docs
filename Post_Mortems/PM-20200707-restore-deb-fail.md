## PM-20200707: restore deb fail

À la suite d'un déploiement, des erreurs peuvent surgir, visibles sur le channel `#technique`. Que faire dans ce cas ?

### Restore du précédent .deb

En se connectant en ssh sur la machine de déploiement `ysc_deploy`, on exécute la commande `fab prod deploy:restore=True`, on sélectionne l'avant-dernier .deb le plus récent (#2) et on attend que le déploiement se fasse.

Mais il peut arriver qu'une mauvaise commande soit exécutée à la place. Ex: `fab locale deploy:restore=True`, ce qui n'est pas bon car on va alors essayer déployer le code sur cette même machine de déploiement. Git se retrouve sur une version détachée du code et non sur la branche `master` qui est celle que l'on va utiliser pour créer le .deb à installer sur les machines de production.

### Fix post installation locale (branche détachée)

Ce qu'on veut au final, c'est avoir le dernier état de `master` sur le serveur `ysc_deploy`. On va le faire de manière drastique.

Pour ce faire :

Depuis le répertoire HOME sur `ysc_deploy`

1. Supprimer le repo `/home/ubuntu/jelouemoncampingcar`
2. Cloner le repo depuis gitlab `git clone -b master --single-branch --depth=1 jeloumoncampingcar`
3. Créer les liens symboliques de prod :
    - `cd jelouemoncampingcar`
    - `ln -s environment_settings/prod_settings.py local_settings.py`
    - `ln -s environment_settings/deploy_settings.py personal_settings.py`
4. Recrééer un environnement fonctionnel :
    - `virtualenv .`
    - `. bin/activate`
    - `pip install --upgrade pip`
    - `pip install --upgrade setuptools`
    - `pip install -r requirements --exists-action=w -q`
5. S'assurer que Django est bien installé en lançant un shell : `./manage.py shell_plus`
    - Si fail (souvent à cause d'un fichier static manquant) : copier le dossier `static` d'une machine locale vers `ysc_deploy` : `scp -r <LOCAL_PROJECT_ROOT>/static/ ubuntu@ysc_deploy:/home/ubuntu/jelouemoncampingcar/` 

Une fois ces étapes terminées avec succès, le serveur de déploiement est de retour à la normale. Danse ce cas, un déploiment peut être lancé depuis une machine locale avec la commande `fab locale release=<numero.de.release>`.

On peut vérifier le bon état des machines de production à l'aide de la commande `fab prod get_code_version`.

### Comment éviter cela

Une des possibilité est de bloquer l'exécution de certaines méthodes sur des serveurs particuliers, telles que la tâche de déploiement appelée en local sur le serveur `ysc_deploy`. 