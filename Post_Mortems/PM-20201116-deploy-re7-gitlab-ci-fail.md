## PM-2021???? Déploiement sur re7 depuis gitlab échoue

### Un nettoyage de fichier de build mal configuré efface une partie de la re7

#### Situation
Les configurations nginx, supervisor on été supprimées (par suppression des paquets stow d'origines situés dans `~/config`) ainsi que des fichiers de paramétrages $USER. Impossible de lancer des processus supervisor, serveur inatégnable depuis AWS Route53.

#### Origine
la connexion ssh depuis le docker gitlab ne semble pas prendre en compte les settings de bash de la racine. Le fichier `~/.profile` ne semblait pas être chargé car l'exécutable node pointait sur `/usr/bin/node` au lieu d'être géré par nvm.

Les tentatives pour charger ces fichiers sont restées infructueuses. Au cours de la dernière tentative, le script de déploiement s'est lancé depuis le mauvais répertoire. Ce script s'occupe de nettoyer l'ancien build, dézipper le nouveau build, et d'installer les dépendances. La partie qui contient le nettoyage était mal écrite car elle se basait sur le répertoire courant au lieu d'un chemin relatif bien indiqué.

En conséquence une partie de la re7 a été effacée sans que l'on puisse avoir le détails. Il semblerait que le nettoyage se soit lancé depuis le repertoire home.

#### Fix

- Redéployer la re7 depuis une image de backup

#### Actions à mener
- Sécuriser le nettoyage de l'ancien build
  - Ne pas laisser Gitlab s'occuper de cette tâche
  - Déplacer le build dans un répertoire temporaire et laisser à un cron le soin de faire le ménage
- Charger les settings bash à travers la connexion ssh du docker Gitlab
- Créer régulièrement des Amazon Machine Image pour backup