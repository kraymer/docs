# PM paiements prélevés deux fois

## Situation

En cours

## Description

Certains Guest qui ont effectué plusieurs tentatives de paiements se voient au final prélevés deux fois

## Origine

![Selection_999_496_](uploads/25494db0f7834872012ec7b1d3aab3b0/Selection_999_496_.png)

La 1e occurence concerne le booking 636053 : https://www.yescapa.fr/ryansimmons/mangopay/mangopaypayin/?q=636053
2 payins SUCCEEDEED, un exécuté à 16h54 l'autre à 19h26

Cela désigne le déploiement v3.22.14 du 28/05 à 16h46 comme ayant certainement introduit le bug.

Le commit 9bff5fb182 correspondant au merge de la branche `reduced_fees_corona_next_booking` est visé.

## Fix 

Le commit a3e2f12ed6 (Mon Jun 1 10:25:54) revert le fait qu'on déclare les paiements de 0e comme ok dans le cas où un crédit location a fait passé le prix d'un booking à 0e.

Le lien entre ce bout de code et le bug n'a pas été établi mais c'était la seule piste et il a été décidé de déployer ce commit en monitorant les cas de double payins.

## Monitoring

La tâche _"X BP with duplicated success payins"_ a été exécutée plusieurs fois au cours de la journée

![Selection_999_497_](uploads/217b635ac5ceadda875246f0255dc2f8/Selection_999_497_.png)

A chaque exécution le nb de payins concerné allait en baissant, aucun nouveau payin apparu d'une exécution à l'autre.
Ces deux constats ont été interprété comme un signe que le fond du problème était résolu.

## Tâche look_for_payment_in_suspend

Ce matin 2 Juin à l'exécution de 6h40, 18 nouveaux cas de paiements doublons sont apparus.
A 6h35 on a la tâche qui fait les paiements 2x en attente qui s'exécute, c'est elle qui exécute les paiements doublons.
C-a-d qu'on a des bookings au cours de la journée qui vont avoir deux payins, un succeeded et l'autre dans un état à définir. Le monitoring ne capte pas ces cas lorsqu'ils arrivent car à ce moment là les 2 payins ne sont pas en succeeded.
A 6h35, le paiement doublon est exécuté, le monitoring le remonte 5 min plus tard. Trop tard.

## Actions à faire

- désactivation de la tâche des paiements en attente : fera juste de l'affichage
- modifier tâche monitoring pour tracker dans quel état est le second payin
- trouver le fond du pb ? `git diff 33e7a916 9bff5fb18`



  
