## How to update mangopaysk package

### Differents SDK versions

Sometimes in the past, MangoPay released the version 3 of their python SDK, 
breaking compatibility with the legacy SDK (versions 2+ < 3).
Mangopay also changed the name of the package, from `mangopaysdk` to `mangopaysdk_v2`.

We still use the legacy SDK. 

In order to avoid names conflict (between the application `apps.mangopay`, the legacy SDK `mangopaysdk` and the new SDK `mangopay`), we forked the SDK to change their package names:
- Legacy SDK: `mangopaysdk` -> `mangopaysdk_2`;
- Official SDK: `mangopay` -> `mangopaysdk`.

|SDK Version | MangoPay package name | Yescapa fork package name
--- | --- | ---
|2+ | mangopaysdk | mangopaysdk_2
|3+ | mangopay | mangopaysdk


### Useful links

- Official Mangopay SDK repository: https://github.com/Mangopay/mangopay2-python-sdk
- Yescapa fork of the official SDK: https://gitlab.com/yescapa-pub/mangopaysdk3
- Yescapa fork of the legacy SDK: https://gitlab.com/yescapa-pub/mangopaysdk


### Syncing the repositories

When MangoPay releases a new version of their SDK, we need to update our fork to
import new changes.

To do so:

- Clone the fork repository:

```
git clone git@gitlab.com:yescapa-pub/mangopaysdk3.git
cd mangopaysdk3
```

- Add an upstream and fetch it:

```
git remote add upstream https://github.com/Mangopay/mangopay2-python-sdk.git
git fetch upstream
```

- Then merge it:

```
git merge upstream/master
```

There might be merge conflicts, fix them.

In case of doubt, this GitHub doc is useful: https://ardalis.com/syncing-a-fork-of-a-github-repository-with-upstream/ .


### Testing the package

Create a virtualenv:

```
python2 -m virtualenv .
source bin/activate
```

Install `tox`:

```
pip install tox
```

Launch the tests:

```
tox
```

(It might fail with the py34 if you do not have it installed on your machine.)


### Syncing

If the tests are ok:

```
git push
```

### Using the last version in the `website` project

The version used by the `website` project is pinned in `requirements/prod.txt`.
So there is no risk for the production if you push to `master` (on the `mangopaysdk3` project).

To test that the change of version do not broke the `website` project, you need to change the 
dependency in the `requirements/prod.txt` file and then launch the whole test suite (be sure to
include MangoPay tests).

If everything is ok, you can submit a Merge Request.