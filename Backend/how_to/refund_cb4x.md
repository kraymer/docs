## Déclencher remboursement partiel d'un paiement cb4x

### Input

Johan vous communique un id de booking et son nouveau montant eg

~~~
Pourrais-tu effectuer le refund CB4X sur la résa 659334, nouveau montant : 123.45€
~~~

### Action

Faire un appel d'api `service_update_order` à partir d'un shell_plus de prod : 

~~~
In [1]: from decimal import Decimal

In [2]: from yescapa.cb4x.models import Cb4xApi

In [3]: bkg=Booking.objects.get(pk=659334)

In [4]: bkg
Out[4]: <Booking: Réservation 659334 : Adria 577 Sp 27583 de Xavier ortiz par Inacio Tavares Da Silva du 2020-07-24 09:00:00 au 2020-07-31 20:00:00 pour 999,60 €>

In [7]: Cb4xApi().service_update_order(bkg, Decimal("999.60"), Decimal("123.45"))
~~~

Le montant initial à utiliser est le montant "Déblocage" du booking en question, visible via le BO `cb4x/cb4xpaymentrecord`


#### Erreur de token

Si jamais vous avez une erreur de token sur cet appel :

~~~
In [7]: Cb4xApi().service_update_order(bkg, Decimal("999.60"), Decimal("123.45"))
2020-06-23 10:20:53,247 yescapa.cb4x.models.service_update_order L108 [INFO] 659334: update_order 99960 => 12345
Out[7]: 
{
    'OrderRef': '659334',
    'OrderTag': None,
    'MerchantID': 57L,
    'MerchantSiteID': '7038',
    'ResponseCode': '1',
    'ResponseMessage': 'The provided scoring token does not match the scoring token used for the order.',
    'Schedule': {
        'ScheduleItem': []
    }
}
~~~

Alors envoyer un mail à casino pour obtenir le token exact (le token utilisé se trouve dans les extra_data) : 

> Subject: Erreur "The provided scoring token does not match the scoring token used for the order"  
> To: contact@support.cb4x.fr  
> Cc: johan@yescapa.com  
>   
> Bonjour,
>
> J'ai l'erreur mentionnée ci dessus lors d'une tentative d'update order.
> il s'agit de l'order ref 659334
> Le scoring token à ma disposition est le aef7a1c3-5d99-4b29-820e-a4133e30c1e1
> 
> Pourriez vous me communiquer le scoring token correct svp ?
> d'avance merci

A reception du token, l'enregistrer dans les `extra_data` du booking et refaire l'appel :

~~~
In [8]: bkg.extra_data["cb4x"]["scoring_token"] = "7e93ffb3-d78e-41d1-87fa-444babb4788d"

In [9]: bkg.save()

In [10]: Cb4xApi().service_update_order(bkg, Decimal("999.60"), Decimal("123.45"))

2020-06-23 10:21:44,690 yescapa.cb4x.models.service_update_order L108 [INFO] 659334: update_order 99960 => 12345
Out[14]: 
{
    'OrderRef': '659334',
    'OrderTag': None,
    'MerchantID': 57L,
    'MerchantSiteID': '7038',
    'ResponseCode': '0',
    'ResponseMessage': 'Request was successfully processed',

~~~

### Result

On peut constater la programmation des remboursements coté Casino suite à l'exécution de la commande via le BO :

![](https://gitlab.com/Yescapa_dev/website/-/wikis/img/view_payment_schedule.png)