# API

API is powered by **Django Rest Framework**.

Authentication is managed by **django-oauth-toolkit**.

Swagger documentation is generated automatically by **drf-yasg**.

There are 3 APIs:

- Web (Legacy)
- Mobile API (4 versions)
- Partners

Mobile and Partners APIs have their own settings and urls files.

## Web (Legacy)

Has been developped waiting for `api.jelouemoncampingcar.com` to be ready.

Used only by old Website.

Urls are declared into `yescapa.rental.urls.py`.

## Mobile

Used by Mobile apps and new Nuxt website.

Declared into a separate file `jelouemoncampingcar.api_urls` wich is the root file for API urls. Then each app can contain several files like `api_urls.py` and `api_urls_v<version>.py`, which are included into the root file.

Run:

    python manage.py runserver --settings=jelouemoncampingcar.api_settings

Documentation is for development purposes for now, you have to set `API_DOCUMENTATION` to `True` into you local settings:

    http://127.0.0.1:8000/documentation

## Partners

Developed for professional owners (still in development).

Urls are declared in `yescapa.rental.public_api_urls`, then each app can contain a file `public_api_urls.py` and `api_urls_common.py` for shared views with our API.

Run:

    python manage.py runserver --settings=jelouemoncampingc.public_api_settings

Documentation:

    http://127.0.0.1:8000/public/v1/documentation/

## Basic principles

Django Rest Framework is a powerful framework for API development.

Basic principles are:
- API views control objects access and permissions.
- If you want to add filters on your view, use Django Filter (you will get them on the swagger documentation).
- Serializers control data returned by the view and data validation.

## Nomenclature

API v4 and Partners API are pretty similar but there is often small differences like object access or serializer format.

So in order to factorize code and know which API you are working on, the following nomenclature has been set for API views:

`Common<Models><Operations>APIView(...)`

Used when behaviour are exactly the same.

`Abstract<Models><Operations>APIView(...)`

Abstract class containing common behaviours, not aim to be use alone as an API view.

`<Model><Operations>APIView(Abstract<Model><Operations>APIView)`

Inherits from the abstract API view and contains API v4 specific behaviour.

`Partners<Model><Operations>APIView(Abstract<Model><Operations>APIView)`

Inherits from the abstract API view and contains partners API's specific behaviour.

Serializers follow the same Prefix rules.

## Deprecation

Main difficulty with API for mobiles is deprecation, we have to manage backwards compatibility with old mobile versions. For that some comments are added into api view or serializers.

`#DEPRECATED` - Deprecated but we don't know exactly when clients will migrate to the new version.

`#DEPRECATED: x.xx` - Not use anymore since mobile version `x.xx`, you have to check (mobile applications versions deprecation)(https://www.yescapa.fr/ryansimmons/mobile_apps/applicationversion/) in order to know if you can delete.

## Resources

[https://www.django-rest-framework.org](https://www.django-rest-framework.org/)

[https://django-filter.readthedocs.io](https://django-filter.readthedocs.io/en/stable/)

[https://drf-yasg.readthedocs.io](https://drf-yasg.readthedocs.io/en/stable/)

[https://django-oauth-toolkit.readthedocs.io](https://django-oauth-toolkit.readthedocs.io/en/latest/)
