## Traitement des alertes

### tech-alert

#### Booking with AA part without BP with AA part
  
- si pas de composante "cancel insurance" sur le BP : editer le booking pour passer le champ `price_cancel_insurance` à 0

#### _site_ is down

Ds le cas général, se résoud automatiquement via lancement d'une nouvelle machine par l'autoscaler.

Si le downtime persiste alors :
- checker si les nvelles instances se lancent bien via l'onglet _EC2
Auto Scaling groups > Activity_ de l'autoscaling group concerné.
Le nb d'instances max de l'autoscaling group  a peut-etre été atteint ? des erreurs peuvent survenir si la launch config paramétrée sur l'autoscaler a été supprimée, etc
- les instances se lancent mais le code du deb est buggé et génère des erreurs applicatives : envisager un restore deb ou deploy d'un fix

### tech-warning

#### BP not done with payin SUCCEEDED

Cette alerte signifie qu'un booking paiement n'a pas le statut attendu alors qu'un payin a été payé correctement.

Ce genre de cas entraîne souvent un mauvais statut au niveau du Booking. E.g, le booking devrait être confirmé mais ne l'est pas.

Deux cas de figure général possible pour cette alerte:

- interventions OPS avec modification manuelle de quelque chose qui amène à un mauvais statut. Dans ce genre de cas la résolution est généralement simple:
  - lecture des commentaires du bookings pour vérifier ce qu'il s'est passé
  - demande de précision auprès des OPS si un cas n'est pas clair
  - passage du statut du BP vers l'état 'Finalisé'
- bug quelque part lors du traitement du paiement. Nécessite une analyse du problème. Etape de résolution typique:
  - ré-éxecuter le split pour faire popper l'erreur associée (payin -> select payin -> actions -> resplit payins). Check Sentry ou papertrail
  - si l'erreur ne pop pas, se rendre directement sur ysc_deploy -> shell_plus -> `mgp_tasks.split_transfer(payin)`. Regarder quelle erreur sort.
  - prévenir les ops du pb si nécessaire
  - passer le booking en CONFIRMED si il devrait l'être au plus vite pour éviter un pb d'Overlap
  - résoudre le pb sous-jacent et au choix :
    - réexécuter le split pour une résolution propre
    - mettre le statut du BP en 'Finalisé' pour une résolution sale et pour éviter que l'alerte ne pop à nouveau le lendemain

Il n'y a plus de bugs de paiements majeurs actuellement, mais il y a quand même des petits bugs qui passent régulièrement dans les mailles du filet à traiter au fur et à mesure.