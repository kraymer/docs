## Dev processes

### Submitting code

- Move jira card to _In progress_ column
- create a feature branch using following nomenclature : <parent branch shortcut>/<jira id>/identifier eg _hot/REN-1234/payout_warning_
- if card instructions are not clear, start a discussion in card comments
- if you need some insights from coworkers, open a MR with `RFC:` prefix (request for comments)
- when code is ready to review, open an MR named as <jira id>: identifier eg _REN-1234: Warnings on payouts fail_
- iterate on reviewers comments
- when all discussions threads are resolved and a reviewer approved the MR, click _Merge when pipeline succeeds_ button
- edit the ongoing changelog at https://gitlab.com/Yescapa_dev/website/-/wikis/hotfix/edit (update url for develop branch)