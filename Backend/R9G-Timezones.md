## Refactoring Timezones

### Situation

Actuellement, nos serveurs évoluent sur deux timezones. L'API et le .com sont en **UTC** alors que les autres sont en **UTC+1**. Nous avons donc en base de données des timestamps dits naïfs avec des horaires dans des timezones différentes. Ils ne contiennent pas l'attribut `tzinfo` qui permet de connaître la timezone du `datetime`.

### Recommandations

Il est recommandé dans la documentation Django de stocker en base de données des `datetime` avec l'information de la timezone et tout cela en **UTC**. Cela signifie que l'on va traiter avec des horaires **UTC** puis convertir à l'affichage pour l'utilisateur.

### Refactoring

- `USE_TZ = True`
- `TIMEZONE = "UTC"`
- `datetime.datetime.now()` -> `timezone.now()`
- Reprendre l'algo `compute_days`
- Avoir la timezone du `vehicle` sur les datetime du `booking`
- Ne plus utiliser les fonctions `get_rounded_hour_*`

Pour connaître le fuseau horaire d'un utilisateur ou d'un véhicule, plusieurs options:
- [Middleware](https://docs.djangoproject.com/fr/1.11/topics/i18n/timezones/#selecting-the-current-time-zone) proposé par Django pour les utilisateurs anonymes.
- Demander à l'utilisateur sa timezone
- En fonction des données de localisation (Coordonnées du véhicule par exemple). Cela avait été essayé l'année dernière avec le package [tzwhere](https://github.com/pegler/pytzwhere) (celui-ci est un fork de l'original). Ce package n'a pas été mis à jour depuis 3 ans. En conséquence, un autre pakckage est apparu, [TimezoneFinder](https://github.com/MrMinimal64/timezonefinder). Adam Johnson qui est membre du board technique de Django contribue à ce package et il est maintenu.

### Base de données

En ce qui concerne la base de données, voici ce que dit Django

> Le moteur PostgreSQL stocke les dates/heures comme timestamp with time zone. En pratique, cela signifie qu’il convertit les objets datetime en UTC à partir du fuseau horaire de la connexion au moment du stockage, et à partir d’UTC vers le fuseau horaire de la connexion lors de la sélection.

>En conséquence, si vous utilisez PostgreSQL, vous pouvez librement passer de USE_TZ = False à USE_TZ = True et inversement. Le fuseau horaire de la connexion de la base de données sera défini respectivement à TIME_ZONE et à UTC, ce qui fait que Django reçoit des dates/heures correctes dans tous les cas. Vous n’avez pas à convertir les données.

La documentation explique comment Django réagit lorsque les conversions ne fonctionnent pas avec des utilitaires dans `django.utils.timezone`  pour gérer la compatiblité.

Dans le blog post, il est dit que l'on doit convertir toutes les heures en base de données (hors celery, etc...).

### API

Pour l'API, deux cas sont présentés dans le blog post:

- Soit le serveur est en charge de définir la timezone courante (par exemple via un champs timezone dans le user), et donc on envoie à l'application mobile l'horaire à afficher.
- Soit la timezone est détectée par le téléphone, envoyée vers Django qui se charge de stocker en UTC. Lors de l'envoie de l'heure à l'application mobile, il faudra que l'application convertisse en fonction de la timezone du téléphone.

La deuxième solution semble plus pratique car les personnes voyagent entre les timezones.

### Sources

- [Blog Post](https://blog.david-dahan.com/tout-comprendre-des-timezones-dans-django-65b4123838dd)
- [Django Documentation](https://docs.djangoproject.com/fr/1.11/topics/i18n/timezones/#migration-guide)
