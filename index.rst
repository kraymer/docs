.. docs documentation master file, created by
   sphinx-quickstart on Wed Sep  1 15:14:08 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Non-code documentation
======================

.. toctree::
   :titlesonly:
   :maxdepth: 2
   :caption: Contents:

   Backend/index.rst
   Devops/index.rst
   HR/index.rst
   Post_Mortems/index.rst
   Security/index.rst

