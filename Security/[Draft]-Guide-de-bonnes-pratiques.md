## Guide de bonnes pratiques

### Introduction

De par la nature de nos services, Yescapa héberge des données utilisateurs très sensibles. En cas d'incident de cybersécurité, cela fait courir un risque important à la survie de Yescapa et à la vie privée de nos utilisateurs. On pense en particulier aux documents que les utilisateurs nous envoient qui peuvent servir aux criminels pour des fraudes.

Avec le télétravail qui se généralise chez Yescapa, il est impératif de suivre quelques bonnes pratiques. Ces dernières sont applicables en entreprise, mais également dans la vie privée pour se protéger.

Nous allons voir dans ce document différentes méthodes utilisées par les cybercriminels et comment réduire le risque qu'un incident survienne grâce à des outils et des bonnes pratiques.

### Mots de passe et authentification à deux facteurs

#### Comment choisir un mot de passe ?

Vos mots de passe doivent répondre à différents critères:
- Il doit contenir au moins 12 caractères.
- Il doit être composé de minuscules, majuscules, chiffres et caractères spéciaux.
- Il ne doit pas contenir d'informations personnelles (nom, date de naissance, etc...)

Si l'authentification multifacteurs est disponible pour le service que vous utilisez, il est fortement recommandé de l'activer. Que ce soit par SMS ou en utilisant une application mobile comme LastPass Authenticator ou Microsoft Authenticator.

#### Méthodes de mémorisation

Si vous souhaitez vous souvenir de certains mots de passe, voici deux moyens mnémotechniques proposés par l'ANSSI (Agence Nationale de la Securite des Systemes d'Information):

- La méthode phonétique :
  - « J’ai acheté 5 CDs pour cent euros cet après-midi » : ght5CDs%E7am
- La méthode des premières lettres :l
  - « Allons enfants de la patrie, le jour de gloire est arrivé » : aE2lP,J2Géa!

Il est également possible d'avoir pour mot de passe, une _passphrase_. C'est à dire une séquence de mots communs qui peut faire plus de 20 caractères et qui est plus facilement mémorisable, par exemple:

- hOwlfRequencynIghtsUnset!

Le choix des mots doit être aussi aléatoire que possible et respecter les critères mentionnés précédemment.

#### Gestionnaires de mots de passe

Mot de passe maitre / mot de passe unique pour chaque compte donc compromission limitee.

Avec la multiplication des comptes sur des plateformes différentes, se souvenir des mots de passe pour chacune peut être difficile. Dans ce cas, il existe des services qui permettent de générer et sauvegarder des mots de passe complexes uniques pour chacun de vos comptes.

Quelques exemples de gestionnaires de mots de passe:

- Dashlane
- NordPass
- Bitwarden
- 1Password
- LastPass

#### Choses à éviter

**[TODO]**

Pour se rappeler de ses identifiants, on peut être tenté de l'écrire sur un post-it ou encore de le sauvegarder dans un fichier que l'on garde sur notre poste de travail. Il est fortement déconseillé de faire cela. Il en va de même avec le fait de s'envoyer ses identifiants par email.
_Pour exemple, en 2015, pendant un reportage sur France 2 chez TV5 Monde, des post-it avec les mots de passe des comptes de réseaux sociaux de TV5 Monde apparaissent à l'écran._

Lorsque vous avez besoin de communiquer un mot de passe à une personne de l'entreprise, il ne faut pas le faire via Slack ou email. _Alternative?_

### Ingénierie sociale

**[TODO]**

- Sentiment d'urgence
- Se faire passer pour un partenaire ou un utilisateur.

#### Emails

**[TODO]**

- Vérification de l'expéditeur. Contacter la personne ou l'organisme directement s' il y a une suspicion quant à la demande ou au contenu de l'email.
- Pièces jointes
  - Documents office (macro)
  - Autres documents
- Grammaire et orthographe
- Liens

#### Téléphone

**[TODO]**

### Appareils et WIFI

**[TODO]**

- Ne pas connecter un appareil personnel au réseau de l'entreprise.
- Si votre téléphone est un outil de travail (accès à vos mails, administration du site, etc...), il faut faire preuve de la même attention que pour votre poste de travail.
- Pendant les evenements publics, faire attention wifi gratuit, vpn

### Bons réflexes

**[TODO]**

- Verrouiller le système lorsqu'on quitte son poste
- Essayer de ne pas communiquer d'identifiants par email ou via slack
- Ne pas connecter de support de stockage (Clés USB, disques durs externes) dont on est pas propriétaire

### Navigateurs

**[TODO]**

Les navigateurs sont également la cible d'attaques pour voler les données des utilisateurs, et dans le pire des cas, prendre le contrôle de la machine. Pour se protéger lorsque l'on navigue:
- Limite les extensions de navigateur au minimum nécessaire en fonction de vos besoins.
- Vous pouvez installer les extensions suivantes pour la vie privée et la sécurité:
  - HTTPS Everywhere
  - Do Not Track

Autres idées à introduire dans le document:
- Perte ou vol d'un appareil (_Appareils et WIFI_)
- Tentatives de fraudes (eg: IBAN)

Credits: [ANSSI](https://www.ssi.gouv.fr/uploads/2017/01/guide_cpme_bonnes_pratiques.pdf)

