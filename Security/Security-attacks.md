## Security attacks

### SMS Spam

#### 2021-08-25

###### Report

Attack is ongoing. It has evolve to be more adaptable:
- IP address is not the same (batch 10-50 phone verification per ip)
- visibility is low with sentry alert with Malicious tag
- This imply a switch in user between phone verification batch
- Batch account creation have been noted. Typical pattern:
  - User country: US
  - random name
  - random address
  - telephone number include previous target (bebbicell network)
- Possible other fraudulent account creation not as obvious (need confirmation) detected from AWS Athena

A few observation where made on Athena:
- User Agent is very similar in case of phone fraud (may be useful for other malicious activity detection):
  - `Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.164 Safari/537.36` (certification count: 38629)
  - `Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36` (certification count: 1674)
  - `Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:88.0) Gecko/20100101 Firefox/88.0` (certification count: 200+)
  - A few other smartphone related UA

###### Mitigation

- To avoid massive user creation, we now make a basic check on email validity and conformity [in progress]: https://gitlab.com/Yescapa_dev/website/-/merge_requests/2159
- To search easily in Aws Athena and ban automatically IP based on the results, a few function and helper were created: https://gitlab.com/Yescapa_dev/website/-/merge_requests/2161
- 2 new IP set have been created on Amazon side: 
  - `Auto-ban` for automatic IP ban from shell plus. Not meant to be eternal.
  - `Permanent-auto-ban` for permanent automatic IP ban if a clear pattern is detected.

#### 2021-07-28

###### Report

During the months of June and July 2021, an attacker has been abusing our phone number verification endpoint to spam Irakian and Swiss phone numbers. It is entirely possible that other operators from different countries could have been targeted but nothing too significant in terms of numbers.

June

| Operator     | Country     | Messages |
| ------------ | ----------- | -------- |
| Asiacell     | Iraq        | 4        |
| Zain Iraq    | Iraq        | 6        |
| Bebbicell AG | Switzerland | 12394    |

July

| Operator     | Country     | Messages |
| ------------ | ----------- | -------- |
| Asiacell     | Iraq        | 12938    |
| Zain Iraq    | Iraq        | 25188    |
| Bebbicell AG | Switzerland | 39680    |

The attacker seems to be using only one IP: `188.225.254.122`. The attacker also sent those requests with 2 user agents: `Android/8.1.0` and `Android/7.1.2` and so probably two smartphones. [Sentry](https://sentry.io/organizations/yescapa/issues/2538000846/events/oldest/?project=30597&query=is%3Aunresolved) logs containing details about the requests.

###### Motivation

The motivation of the attacker is not clear. Two scenarios are possible:
- Make the company run out of budget on our Nexmo account.
- DDOS the services of the different operators targeted.

###### Mitigation

The fastest answer was to stop sending SMS to Irakian numbers since it's highly unlikely that we'll have a lot of genuine requests. The same limit could not be put for the Swiss numbers so a software limit was put in place to prevent someone from asking a verification code more than 10 times in a week. For each requests, even if we do not send the message, we return a `200 OK` to make it seem like everything is working well. It looks like it is working because the attacker doesn't seem to have a number to test if his attack is actually working.

In the long term, what was discussed was recommended, meaning it should be treated as a DDOS attack (20req/sec) and blocked via the [Nginx configuration](https://nginx.org/en/docs/http/ngx_http_limit_req_module.html#example). If the ISP the attacker is using doesn't allow for dynamic IP change, and he doesn't use a VPN, an IP ban might be the solution when the limit of request number is met.