## Deploy procedure

### Prerequisites

- Having public keys of `ysc_deploy`, `ysc_ami` (For the snapshot)
- Clone wiki into website
```
git clone git@gitlab.com:Yescapa_dev/website.wiki.git
```
- Check buildbot is ok for the branch deployed
- Check traductions are ok by running:
```
./manage.py test --settings=jelouemoncampingcar.environment_settings.tests_settings i18n.tests.buildbot_tests_i18n.I18NBranchTestCase.test_branch_translated
```

### Inform

Depending on which branch you are going to deploy, put a message on #tech:
```
@here :build-develop: :051-departure:
```
Or
```
@here :build-hotfix: :051-departure:
```

After 5 min, if nobody say "STOOOP !!", you can launch deployment.

### Deploy

Checkout the branch you want to deploy, then pull.

```
git checkout hotfix
git pull
```

Then launch deployment (check tags on https://gitlab.com/Yescapa_dev/website/-/tags)

```
fab locale release:version=x.xx.x
```
_NB: "v" prefix will be automatically added to the version number when creating the tag._

The release command will stop and ask you to connect to ysc_deploy and launch deploy.sh script. Proceed as instructed.

If the script `deploy.sh` crashes during deploy, you can run isolated task without
starting from scratch, check `deploy.sh --help` usage.

### Check the result

Before `get_code_version`, pull hotfix in order to have the new tag.

```
fab prod get_code_version
```

### Troubleshootings

In case of some instances haven't the right version (it happens with autoscaling)

```
ssh ysc_deploy
~/deploy$ deploy.sh -d
```

Then select the `.deb` you want to restore.

For other tricky errors, see: https://gitlab.com/Yescapa_dev/website/-/blob/master/sphinx_docs/FABFILE.md

### Rollback

Manually delete the failed .deb from the _jlm-deb_ S3 bucket, so that fresh launched instances don't download it on startup.  
Then run from ysc_deploy :

~~~
fab prod deploy:restore=True
~~~

### Isolated instances

We have some services running under python 3, you have to deploy them into a second time.

Both command below might fail with:

```
UnicodeDecodeError: 'ascii' codec can't decode byte 0xe2 in position ...: ordinal not in range(128) error.
```

Don't worry, everything's fine. Just rerun this command until it is done without error.

```
fab py3_secondhand py3_deploy
```

### Notify

If everything is ok notify the team.

```
fab locale deploy_slack_done
```

### Create the snapshot

```
ssh ysc_ami
fab locale snapshot
```

Creating the snapshot is long so we got a timeout. You can monitor snapshot creation on AWS.
As soon as snapshot is ready, re-connect to `ysc_ami` and re-do `fab locale snapshot`.