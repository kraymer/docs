## AWS Costs optimizations

### EBS

* Delete non attached and non named EBS volumes : Go to [https://eu-west-1.console.aws.amazon.com/ec2/v2/home?region=eu-west-1#Volumes:attachmentStatus=detached;!tag:Name=;sort=desc:createTime](https://eu-west-1.console.aws.amazon.com/ec2/v2/home?region=eu-west-1#Volumes:attachmentStatus=detached;!tag:Name=;sort=desc:createTime) Select all Check that are available Delete
* Delete old EBS snapshots : Go to [https://eu-west-1.console.aws.amazon.com/ec2/v2/home?region=eu-west-1#Snapshots:visibility=owned-by-me;sort=desc](https://eu-west-1.console.aws.amazon.com/ec2/v2/home?region=eu-west-1#Snapshots:visibility=owned-by-me;sort=desc) Delete all snapshots that are older than 1 month except from the 3 firsts snapshots (for History ;))

### AMI

* Deletes old AMIs : Go to [https://eu-west-1.console.aws.amazon.com/ec2/v2/home?region=eu-west-1#Images:sort=desc:creationDate](https://eu-west-1.console.aws.amazon.com/ec2/v2/home?region=eu-west-1#Images:sort=desc:creationDate) Delete all uwsgi AMIs that are older than 1 month and not in-use

### AUTO SCALING

Delete old Launch Configurations Go to [https://eu-west-1.console.aws.amazon.com/ec2/autoscaling/home?region=eu-west-1#LaunchConfigurations](https://eu-west-1.console.aws.amazon.com/ec2/autoscaling/home?region=eu-west-1#LaunchConfigurations) Delete all uwsgi Launch Configurations that are older than 1 month

### INSTANCES

* Terminate any unused instances Go to [https://eu-west-1.console.aws.amazon.com/ec2/v2/home?region=eu-west-1#Instances:!tag:Name=;!tag:aws:autoscaling:groupName=;instanceState=stopped;sort=instanceState](https://eu-west-1.console.aws.amazon.com/ec2/v2/home?region=eu-west-1#Instances:!tag:Name=;!tag:aws:autoscaling:groupName=;instanceState=stopped;sort=instanceState) (Double) Check and terminate instances