## Serveur recette

Lorsque on veut pouvoir tester une branche avec l'api, se checkout sur la branche, puis lancer la commande `fab <serveur_de_recette> checkout:username="api"`.

### Connexion à la machine

Après avoir fait ajouter se clé publique SSH au fichier `$HOME/.ssh/authorized_keys`, se connecter en ssh à `ubuntu@ec2-34-250-87-19.eu-west-1.compute.amazonaws.com`.

### Relance du processus

Faire un état des lieux du statut des processus disponible : `sudo supervisorctl status`.

Relancer le processus qui concerne l'api : `sudo supervisorctl restart uwsgi-api`.