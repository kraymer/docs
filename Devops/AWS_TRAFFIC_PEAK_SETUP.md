## AWS traffic peak setup


### Autoscalers

Progressively increase the min/max threshold of the autoscaler(s)
eg for a national TV show, the autoscaler-fr target size is 16 instances
Use the _Automatic scaling > Scheduled actions_ settings to  program a scale up
if you can't do it live, take care of timezones when setting start/end times.

You can update the size of the cluster by using this command:
> fab locale _update_autoscaling_group_size:autoscaling_group="app-fr",new_size=3


### ES cluster

Progressively increase the number of data nodes in the cluster.
Do it one by one, ES does not like brutal changes to the cluster and you
might risk to pull the whole cluster down.
A reasonnable number of nodes is 8 data nodes in the cluster
(worked well for M6 Zone Interdite in 2020)

You can update the size of the cluster by using this command:
> fab locale _update_elasticsearch_cluster_size:new_size=4


### Elasticache

It might not be necessary, but for big TV shows, it is preferrable
to add 2 replicas nodes in the prod-medium-image cluster.


## Fabric helper (obsolete)

The following command is a helper to prepare the infrastructure
for yescapa.fr and API. It can be adapted for other domains.
> fab locale setup_infra_for_peak_traffic_fr

Verify the command defaults and adapt them for the traffic
peak to come. See above for reasonnable defaults.



## Monitoring

Tabs to open :
- https://rpm.eu.newrelic.com/ Web transactions time
- Elasticache: monitoring dashboard of `prod-medium-image` node
- ES service: monitoring dashboard (cluster health) of `ysc-elastic-prod`
- Google Analytics (active users, page views)
