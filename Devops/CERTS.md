## SSL certificates

### AWS certificates

Aller dans le service "Certificate Manager"

Cliquer sur _Request a certificate_

- Request a public certificate
- Entrer le domain name eg "www.yescapa.com"
- Add another name to this certificate : retirer le sous-domaine www eg "yescapa.com"
- DNS validation :
    * Tag Name: "Name"
    * Value: \<bare domain with dot replaced with hyphen\> eg "yescapa-com"
- Review 
- Confirm and request
- cliquer _Create record in route53_ pour chaque domaine et attendre que le status ne 
 soit plus _Pending_ (5 minutes max)

### Let'sEncrypt certificates

In order to allow https on ysc-gateway, a new certificate has been created on
this very instance.

#### Install packages

First, we need to connect to ysc-gateway, then install letsencrypt certbot app.
Required packages are:

  - certbot
  - python-certbot-nginx

Install them with apt

#### Generate certificate

Execute `sudo certbot --nginx -d <domain_name>`.
Nginx conf related to <domain_name> will be automatically updated and auto
renewal will be part of the configuration. This can be tested using command
`sudo certbot renew --dry-run`
