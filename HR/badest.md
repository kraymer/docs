# Formation stories

BADEST are a set of stories to introduce to a new dev coworker to get him familiar with rental website.

### Dev tools

- shell_plus
- sentry
- aws.sh

### Tests (30m)

- Django lance les tests a partir d'une db vide, les données sont créées par les tests via des fixtures 
- Lancer un test précis : `./manage.py test -v 3 --settings='jelouemoncampingcar.environment_settings.dev_tests_settings' booking.tests.tests_checkouts.BookingCheckoutTestCase.test_visited_countries`
- Introduction au service `buildbot` : https://gitlab.com/yescapa-dev/buildbot/
- Définition des builders de tests : https://gitlab.com/yescapa-dev/buildbot/blob/master/master/master.cfg


### Fabric (1h)

### AWS (1h)

### App `booking` (1h)

Une réservation - _Booking_ - fait le lien enre un locataire - _Guest_ - et un propriétaire - _Owner_. Lorsqu'un utilisateur souhaite faire une réservation sur une annonce, il va lui suffir de choisir une date de départ et une date de retour sur une annonce publiée. La demande est alors envoyée au Owner qui va décider de l'accepter ou non en fonction des lieux de destination du Guest ou autres critères - si toutefois il n'a pas activé la réservation instantanée, validant directement la demande. Une fois acceptée, le Guest a alors la possibilité de payer sa réservation. _Il nous est possible de ne pas faire cette étape en local et de passer directement la réservation dans une état `TO_COME`_. Une fois le paiement effectué, la réservation peut se faire. 

### App `vehicles` (30m)

Lorsqu'un propriétaire crée une annonce sur le site, il va passer par l'onboarding - découpé en 
 8 étapes. Il va tout d'abord choisir le type de véhicule, la marque et le modèle, le nombre de places assises/ceintures de sécurité/couchage puis renseigner sur l'étape suivante toutes les informations propres au véhicule - immatriculation, date et pays de mise en circulation, dimensions, carburant, etc. Une fois cette étape passée, une annonce - _Ad_ - est créée (état `CREATED`) et rattachée au véhicule. Les étapes suivantes servent à donner encore davantage de détails sur le véhicule - photos, équipements, description, lieu de stationnement, tarifs.

L'annonce est alors dans l'état `TO_MODERATE`. C'est aux pôles de vérifier l'annonce et la valider.
Une fois tous les documents véhicules importés - contrôle technique, assurance, carte grise - elle peut être publiée et accessible à tous les utilisateurs de la plateforme.

#### Paiements (1h)

###### Formulaire de la page de _Demande_ (`booking/views.py::_process_booking_form`)

On appelle la fonction `ad.calculate_prices` avec les dates et les options kilométriques sélectionnées, et on obtient les différentes composantes de prix que l'on transmet au constructeur de `Booking`. Les bookings sont créés avec le status "ABANDONED" (legacy code).

###### Formulaire de la page de _Checkout_ (`booking/views.py::_process_checkout_form`)

Les différents types de BookingPayment (BP) sont instanciés avec les bon montants, ils ont pour status "SUGGESTED". 
Le Guest choisit un type de paiement qui passe alors en "SELECTED".
Le Guest est envoyé sur le site de sa banque pour effectuer le paiement et revient chez nous via `booking/views.py::stand_alone_payment_confirmation`,  si le paiement est ok on appelle la méthode `execute_payment` sur le BP qui résulte sur la création du payin. C'est lors de cette dernière action que le prélèvement du montant des fees est programmé.

##### Hook `payin_status_changed` Mangopay

Quand le payin passe en "SUCCEEDED", Mangopay nous envoie un payload et on passe dans les fonctions `payin_status_changed_handler -> _manage_transfer` et ultimement dans `split_transfer` ou `cb4x_split_transfer`

`split_transfer:` consiste à effectuer 2 transferts à partir du wallet G où résident les fonds. Transfert #1 envoie la part owner vers le wallet O. Transfert #2 envoie la part assurance + assistance vers le wallet JLM (l'argent sera reversé après aux acteurs concernés via un process manuel).

`cb4x_split_transfer` : les paiements 4x dans notre système sont "comme" des 1x dans le sens où on aura un unique BP de type 4x pour le booking. Contrairement aux autres modes de paiement, ce BP ne donne pas lieu à un payin.
Chaque jour on reçoit sur le wallet Casino un payin dont le montant correspond à la somme des bookings 4x payés la veille. 
C'est ce méga payin qui va être reporté via le hook et splitté suivant l'algo : 
- on récupère la liste des bookings 4x non splittés en les classant suivant leur date de départ (proche -> éloignée)
- pour chaque booking, on fait 2 transferts à partir du wallet Casino : Transfert #1 envoie la part owner vers le wallet O. Transfert #2 envoie la part ysc (assurance + assistance + fees JLM - fees casino) vers le wallet JLM. A noter: on ajoute nos fees car les payins Casino ne sont pas setup avec le champ fees non nul comme c'est le cas pour les payins 1x 2x.


### Re7

Pour tester et faire tester des features, on utilise un environnement de recette par développeur. Pour le monter : 

- re7_settings_dev
- conf supervisor dev
- conf nginx dev
- route 53 AWS