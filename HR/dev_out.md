# In & Out procedures

## Developer joining the team

Below, the list of presentations to do with estimated time.

### Dev tools (1h)

- jira
- shell_plus
- sentry
- aws.sh

### Tests (30m)

- Django lance les tests a partir d'une db vide, les données sont créées par les tests via des fixtures
- Lancer un test précis : `./manage.py test -v 3 --settings='jelouemoncampingcar.environment_settings.tests_settings' booking.tests.tests_checkouts.BookingCheckoutTestCase.test_visited_countries`
- Introduction au service `buildbot` : https://gitlab.com/yescapa-dev/buildbot/
- Définition des builders de tests : https://gitlab.com/yescapa-dev/buildbot/blob/master/master/master.cfg


### Fabric (1h)

TODO

### AWS (1h)

TODO

### App `booking` (1h)

Une réservation - _Booking_ - fait le lien enre un locataire - _Guest_ - et un propriétaire - _Owner_.

Lorsqu'un utilisateur souhaite faire une réservation sur une annonce, il va lui suffir de choisir une date de départ et une date de retour sur une annonce publiée. La demande est alors envoyée au Owner qui va décider de l'accepter ou non en fonction des lieux de destination du Guest ou autres critères - si toutefois il n'a pas activé la réservation instantanée, validant directement la demande.

Une fois acceptée, le Guest a alors la possibilité de payer sa réservation. Il nous est possible de ne pas faire cette étape en local et de passer directement la réservation dans une état `TO_COME` (setting `PAYMENT_ALWAYS_OK`).

Une fois le paiement effectué, la réservation peut se faire.

### App `vehicles` (30m)

Lorsqu'un propriétaire crée une annonce sur le site, il va passer par l'onboarding - découpé en 8 étapes.

Il va tout d'abord choisir le type de véhicule, la marque et le modèle, le nombre de places assises/ceintures de sécurité/couchage puis renseigner sur l'étape suivante toutes les informations propres au véhicule - immatriculation, date et pays de mise en circulation, dimensions, carburant, etc.

Une fois cette étape passée, une annonce - _Ad_ - est créée (état `CREATED`) et rattachée au véhicule. Les étapes suivantes servent à donner encore davantage de détails sur le véhicule - photos, équipements, description, lieu de stationnement, tarifs.

L'annonce est alors dans l'état `TO_MODERATE`. C'est aux pôles de vérifier l'annonce et la valider.
Une fois tous les documents véhicules importés - contrôle technique, assurance, carte grise - elle peut être publiée et accessible à tous les utilisateurs de la plateforme.

### Re7

Pour tester et faire tester des features, on utilise un environnement de recette par développeur. Pour le monter :

- re7_settings_dev
- conf supervisor dev
- conf nginx dev
- route 53 AWS

## Developer leaving the team

- suppress dev public key from ysc-ami in ~/.ssh/authorized_keys
- suppress accounts for sentry, jira, gitlab, aws, trello, slack, yescapa database (@yescapa.com account), google suite
- suppress facebook account from yescapa group
- change passwords for sendgrid, buildbot, facebook (dev@yescapa.com)
- redirect emails to other mailbox
